declare type ConfigType = {
	pageSize: number,
	lineBreak: string,
	downloadPath: string,
}

declare type BookInfo = {
	bookPath: string,
	curPage: number,
}
