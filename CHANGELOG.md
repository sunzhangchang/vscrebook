# 更新日志

## [0.3.0]
- 修复
  - 修复在删除书籍后按老板键可以继续看到内容的问题

- 新增
  - 新增网络下载小说(书源: [采墨阁](https://www.caimoge.net/))

## [0.2.3]
- 修复
  - 修复在删除书籍后可以继续看到内容的问题

- 删除
  - 删除老板键弹出运行和运行失败信息

## [0.2.2]
- 修复
  - 修复在另一窗口阅读的进度更新不同步问题

## [0.2.1]

- 修复
  - 修复 用翻页显示小说后，第二次按下老板键需要按两次才能恢复小说文字的 Bug

## [0.2.0]

- 新增
  - 第二次按下老板键恢复小说文字

## [0.1.1]

> > 发布时间 2021-11-03

- 修复
  - 修复重新激活扩展后已添加书籍记录丢失的 Bug (没有完全去除调试代码)

## [0.1.0]

> > 发布时间 2021-11-02

- 改善

  - 不再从原小说地址读取小说内容，而是将小说拷贝一份储存在插件全局文件位置

- 新增
  - 增加 **图书馆** 功能，支持多本书籍阅读，支持添加书籍，删除书籍
  - 自动转码([支持格式参考 chardet](https://www.npmjs.com/package/chardet))

## [0.0.1]

> > 发布时间 2021-11-01

- 改善

  - 优化翻页响应速度
  - 变成手动刷新
  - 增强跳转页功能
  - 老板键新增 `Kotlin`, `Groovy`, `C#`, `JavaScript`, `TypeScript`, `ReScript`, `PureScript`, `Scala.js` 等语言的 `Hello, World!` 及 弹出运行和运行失败信息

- 删除
  - 删除是否为英文
